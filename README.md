# User Story Text Mining
Text mining user stories to extract features from large projects

## Tests
To execute tests, you must be in the Command Line in the user_story_text_mining directory.

### Full Test Suite
* (Linux) All tests : **./src/run_tests.sh a**
* (Windows) All tests : **./src/run_tests_win.sh a**

### Unit Tests
* (Linux) All unit tests : **./src/run_tests.sh u**
* (Windows) All unit tests : **./src/run_tests_win.sh u**
* (Linux) Dataset unit tests : **./src/run_tests.sh d**
* (Windows) Dataset unit tests : **./src/run_tests_win.sh d**
* (Linux) Preprocessing unit tests : **./src/run_tests.sh p**
* (Windows) Preprocessing unit tests : **./src/run_tests_win.sh p**
* (Linux) Model unit tests : **./src/run_tests.sh m**
* (Windows) Model unit tests : **./src/run_tests_win.sh m**
* (Linux) Clustering unit tests : **./src/run_tests.sh c**
* (Windows) Clustering unit tests : **./src/run_tests_win.sh c**
* (Linux) Data structures unit tests : **./src/run_tests.sh ds**
* (Windows) Data structures unit tests : **./src/run_tests_win.sh ds**
* (Linux) Feature model unit tests : **./src/run_tests.sh f**
* (Windows) Feature model unit tests : **./src/run_tests_win.sh f**

### Integration Tests
* (Linux) All integration tests : **./src/run_tests.sh i**
* (Windows) All integration tests : **./src/run_tests_win.sh i**

## Documentation
To generate documentation, you must be in a terminal in the user_story_text_mining directory.

### Generate documentation
* Run command : **./src/doc.sh**