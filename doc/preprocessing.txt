original_document : Array<String>



preprocess_data_lsi (in Array<String> original_document)
- set to lowercase (string.lower)
- tokenizing (nltk.tokenize.RegexpTokenizer)
- removing stopwords (nltk.corpus.stopwords)
- stemming (nltk.stem.porter.PorterStemmer)
out Array<Array<String>> clean_text

to_dictionary (in Array<Array<String>> clean_text)
- create dictionary (gensim.corpora.dictionary.Dictionary)
out Dict<int, string> dictionary

to_doc_term_matrix (in Dict<int, string> dictionary, in Array<String> clean_text)
- use dictionary to turn clean text to doc-term matrix
out Array<Array<(int, int)>> doc_term_matrix



preprocess_data_lda (in Array<String> orignial_document)
- set to lowercase (string.lower)
- simple preprocessing (gensim.utils.simple_preprocess) : Array<String> -> Array<Array<String>>
out Array<Array<String>> clean_text

to_dictionary (in Array<Array<String>> clean_text)
- create dictionary (gensim.corpora.dictionary.Dictionary)
out Dict<int, string> dictionary

to_doc_term_matrix (in Dict<int, string> dictionary, in Array<Array<String>> clean_text)
- use dictionary to turn clean text to doc-term matrix
out Array<Array<(int, int)>>



preprocess_data_doc2vec (in Array<String> original_document)
- each phrase is given an id [int] and turned into a TaggedDocument
out Array<TaggedDocument(string, Array<int>)>

// TODO - to test
alt_preprocess_data_doc2vec (in Array<String> original_document)
- try putting in tokenized array of strings?
out Array<TaggedDocument(Array<String>, Array<int>)>



TaggedDocument:
- Array<String> words
- Array<String> tags | Array<int> id




























