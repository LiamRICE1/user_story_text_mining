ProjectIssue
{
	'id': integer,
	'iid': integer,
	'project_id': integer,
	'title': String,
	'description': String,
	'state': String, // "opened" or "closed"
	'created_at': datetime,
	'updated_at': datetime,
	'closed_at': datetime,
	'closed_by': Person,
	'labels': Array[String],
	'milestone': ?,
	'assignees': Array[Person],
	'author': Person,
	'type': String, // always "ISSUE" ?
	'assignee': Person,
	'user_notes_count': integer,
	'merge_requests_count': integer,
	'upvotes': integer,
	'downvotes': integer,
	'due_date': datetime,
	'confidential': boolean,
	'discussion_locked': ?,
	'issue_type': String, // "issue" ?
	'web_url': String,
	'time_stats': TimeStats,
	'task_completion_status': TaskStatus
	'weight': integer,
	'blocking_issues_count': integer,
	'has_tasks': boolean,
	'task_status': String, // Stringified verion of 'task_completion_status'
	'_links': Link,
	'references': Reference,
	'severity': String,
	'moved_to_id': ?,
	'service_desk_reply_to': ?,
	'epic_iid': ?,
	'epic': ?,
	'iteration': ?,
	'health_status': ?,
}

Person
{
	'id': integer,
	'username': String,
	'name': String,
	'state': String,
	'avatar_url': String,
	'web_url': String,
}

TimeStats
{
	'time_estimate': integer,
	'total_time_spent': integer,
	'human_time_estimate': ?,
	'human_total_time_spent': ?,
}

TaskStatus
{
	'count': integer,
	'completed_count': integer
}

Link
{
	'self': String,
	'notes': String,
	'award_emoji': String,
	'project': String,
}

Reference
{
	'short': String,
	'relative': String,
	'full': String,
}
