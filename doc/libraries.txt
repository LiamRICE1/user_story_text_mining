	Gensim :
Gensim is a very deep library offering lots of functionality in terms of NLP. It is one of the most popular NLP libraries, and it offers functions for most of the techniques used in this project. These functions also possess high multithreading and distributed computing capabilities. It seems to play well with NLTK's data model, facilitating integration.


	NLTK :
NLTK is a strong NLP library which offers many integrations to other NLP libraries. It provides a large number of corpuses for training, as well as detailed preprocessing methods. Particularly useful are the provided stopwords, the stemmer and the provided perceptron tagger for part-of-speech tagging. It plays well with many other NLP libraries, and seems very useful for preprocessing.


	Scikitlearn :
Scikitlearn is a very deep and complex library containing many interesting methods. I will have to check whether I wish to use its k-means algorithm, or NLTK's. Probably NLTK's as it is part of a library I already use. It has very in depth methods geared towards data science, but is not as specialised towards NLP as Gensim. It seems to play well with NLTK's data model, facilitating integration.


	Orange :
Orange is a data science libary geared towards learning. Most of its processing is oriented towards learning rather than efficient data mining, as it uses a visual programming front-end and high visualisation, but it also provides a scripting interface. It is more geared towards clustering, regression and preprocessing. It does offer some models, like k-means, neural networks and trees. It's preprocessing does not have much depth, and is not geared towards NLP. In fact, most of it's functions seem to be simplified wrappers for sklearn implementations.



