@startuml

class Dataset{
	int pid=12440105
	List<ProjectIssue> issues
	List<String> documents
	List<ProjectIssue> get_issues_from_project(int projectid, bool getAll=False)
	Tuple<List<ProjectIssue>, List<ProjectIssue>> isolate_user_stories_from_issues(List<ProjectIssue> issues)
	List<String> isolate_issue_titles(List<ProjectIssue> issues)
	bool write_to_file_readable(List<ProjectIssue> issues, String filename)
	bool write_to_file(List<ProjectIssue> issues, String filename)
	bool write_strings_to_file(List<String> str_list, String filename)
	List<String> read_from_file(String filename)
}

class Preprocessing{
	List<String> documents
	List<List<String>> clean_text
	List<TaggedDocument> clean_text_vec
	Dictionary<int, String> dictionary
	List<List<Tuple<int, int>>> doc_term_matrix
	download_data()
	List<List<String>> simple_tokenize(List<String> original_documents)
	List<List<String>> complex_tokenize(List<String> original_documents, bool remove_stopwords=True, bool stem=True)
	Dictionary<int, String> to_dictionary(List<List<String>> clean_text)
	List<List<Tuple<int, int>>> to_doc_term_matrix(Dictionary<int, String> dictionary, List<List<String>> clean_text)
	List<TaggedDocument> simple_tokenize_tag(List<String> original_documents)
	List<TaggedDocument> complex_tokenize_tag(List<String> original_documents, bool remove_stopwords=True, bool stem=True, bool pos_tag=False)
}

class Model{
	LdaMulticore lda(Array<Array<Tuple<int, int>>> doc_term_matrix, Dictionary<int, String> dictionary, int num_topics=50, int iterations=10, int seed=None)
	LdaMulticore lda_load_local(String fname)
	LsiModel lsi(Array<Array<Tuple<int, int>>> doc_term_matrix, Dictionary<int, String> dictionary, int num_topics=50, int iterations=10, int seed=None)
	LsiModel lsi_load_model(String fname)
	Array<Tuple<int, float>> get_value(Array<Array<Tuple<int, int>>> document)
	Word2Vec word2vec(Array<Array<String>> document_words, int vector_size=100, int iterations=5, bool use_skip_gram=False, bool use_hierarchical_softmax=False, seed=None)
	Word2Vec word2vec_load(String model_to_fetch)
	Word2Vec word2vec_load_local(String fname)
	Array<float> word2vec_get_vector(String word)
	Doc2Vec doc2vec(Array<TaggedDocument> tagged_documents, int vector_size=100, int iterations=10, bool use_distributed_memory=False, bool use_hierarchical_softmax=False, bool use_dbow_and_skip_gram=False, seed=None)
	Doc2Vec doc2vec_load_local(String fname)
	Array<float> doc2vec_get_vector(Array<String> document)
	float vec_model_loss()
	bool save_model(String fname, LsiModel || LdaMulticore || Word2Vec || Doc2Vec model)
}

class ProjectIssue{
	int id
	int project_id
	String title
	String description
	String state
	datetime created_at
	datetime updated_at
	datetime closed_at
	datetime due_date
	List<String> labels
}

class TaggedDocument{
	List<String> tokens
	List<String> | List<int> tags
}

@enduml