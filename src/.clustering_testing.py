from gensim.models import Word2Vec
from nltk.cluster import KMeansClusterer
import nltk



def m():
    sentences = [['this', 'is', 'the', 'good', 'machine', 'learning', 'book'],
                ['this', 'is',  'another', 'book'],
                ['one', 'more', 'book'],
                ['this', 'is', 'the', 'new', 'post'],
                ['this', 'is', 'about', 'machine', 'learning', 'post'],  
                ['and', 'this', 'is', 'the', 'last', 'post']]

    NUM_CLUSTERS = 3

    model = Word2Vec(sentences, min_count=1)

    X = model.wv[model.wv.key_to_index]


    ### K MEANS IN NLTK ###
    """

    kclusterer = KMeansClusterer(NUM_CLUSTERS, distance=nltk.cluster.util.cosine_distance, repeats=25)
    # can also use kclusterer = KMeansClusterer(NUM_CLUSTERS, distance=nltk.cluster.util.euclidean_distance, repeats=25) but not as good?
    assigned_clusters = kclusterer.cluster(X, assign_clusters=True)

    print(assigned_clusters)

    words = list(model.wv.key_to_index)
    for i, word in enumerate(words):
    	print(word, ":", assigned_clusters[i])

    """


    ### K MEANS IN SCIKIT LEARN ###
    """
    from sklearn import cluster
    from sklearn import metrics
    kmeans = cluster.KMeans(n_clusters=NUM_CLUSTERS)
    kmeans.fit(X)

    labels = kmeans.labels_
    centroids = kmeans.cluster_centers_

    print("Cluster id labels for inputted data")
    print(labels)
    print("Centroids data")
    print(centroids)

    print("Score (Opposite of the value of X on the K-means objective which is Sum of distances of samples to their closest cluster centre):")
    print(kmeans.score(X))

    silhouette_score = metrics.silhouette_score(X, labels, metric='euclidean')

    print("Silhouette score:")
    print(silhouette_score)
    """


### K MEANS TEST WITH MY LIBRARY ###

from cluster import Cluster
from dataset import Dataset
from preprocessing import Preprocessing
from model import Model
import math
import os
import gensim
import gensim.downloader as api
import numpy as np


def main():
    c = Cluster()
    d = Dataset()
    p = Preprocessing()
    m = Model()


    # Fetching documents
    print("\nFetching documents...")
    training_documents = d.read_from_file("data/user_story_training_dataset.txt")
    documents_to_sort = d.read_from_file("data/user_story_datasets/g27-culrepo.txt")
    print("Documents fetched.")

    # Preprocessing documents (LSI / LDA)
    print("\nPreprocessing documents...")
    dictionary, training_dtm, dtm_to_sort = p.preprocess_lsi_lda(training_documents, documents_to_sort)
    # Preprocessing documents (Word2Vec)
    training_tokens, tokens_to_sort = p.preprocess_word2vec(training_documents, documents_to_sort)
    # Preprocessing documents (Doc2Vec)
    training_tags, tags_to_sort, training_toks, toks_to_sort = p.preprocess_doc2vec(training_documents, documents_to_sort)
    print("Documents preprocessed.")

    print("\n\n")

    # Creating model (LSI)
    print("\nCreating LSI model...")
    lsi_vec_size = 50
    lsi, lsi_seed = m.lsi(training_dtm, dictionary, lsi_vec_size, 50)
    # Getting sorted data (LSI)
    lsi_sort_values = m.get_values(dtm_to_sort)
    lsi_training_values = m.get_values(training_dtm)
    lsi_sort_arrays = m.to_ndarray(m.lsi_topics_to_vector(lsi_sort_values, lsi_vec_size), lsi_vec_size)
    lsi_training_arrays = m.to_ndarray(m.lsi_topics_to_vector(lsi_training_values, lsi_vec_size), lsi_vec_size)
    print("LSI model created.")

    # Creating model (LDA)
    print("\nCreating LDA model...")
    lda_vec_size = 50
    lda, lda_seed = m.lda(training_dtm, dictionary, lda_vec_size, 50)
    # Getting sorted data (LDA)
    lda_sort_values = m.get_values(dtm_to_sort)
    lda_training_values = m.get_values(training_dtm)
    lda_sort_arrays = m.to_ndarray(m.lsi_topics_to_vector(lda_sort_values, lda_vec_size), lda_vec_size)
    lda_training_arrays = m.to_ndarray(m.lsi_topics_to_vector(lda_training_values, lda_vec_size), lda_vec_size)
    print("LDA model created.")

    vec_size = 300
    # Creating model (Word2Vec)
    print("\nCreating Word2Vec model...")
    w2v = m.word2vec_load("word2vec-google-news-300")
    # Getting sorted data (Word2Vec)
    w2v_sort_arrays = m.word2vec_get_vectors(tokens_to_sort)
    w2v_training_arrays = m.word2vec_get_vectors(training_tokens)
    print("Word2Vec model created.")

    # Creating model (Doc2Vec)
    print("\nCreating Doc2Vec model...")
    d2v, d2v_seed = m.doc2vec(training_tags, vec_size, 200, True, True, True)
    # Getting sorted data (Doc2Vec)
    d2v_sort_arrays = m.doc2vec_get_vectors(toks_to_sort)
    d2v_training_arrays = m.doc2vec_get_vectors(training_toks)
    print("Doc2Vec model created.")

    # Setting k means attribues
    num_topics = math.floor(len(training_documents)/10)
    num_resets = 200

    print("\n\n")

    # Clustering k means objects (LSI)
    print("\tLSI k-means :\nNumber of clusters =",num_topics,"\nNumber of resets =",num_resets,"\nVector Size =",lsi_vec_size,"\nModel seed =",lsi_seed)
    c.skl_kmeans(num_topics, 100)
    c.skl_kmeans_fit(lsi_training_arrays)
    lsi_classified = c.skl_kmeans_get_clusters(lsi_sort_arrays)
    lsi_sorted = c.sort(documents_to_sort, lsi_classified, num_topics)
    lsi_vec_sorted = c.sort(lsi_sort_arrays, lsi_classified, num_topics)
    lsi_score = c.skl_score(lsi_sort_arrays)
    c.write_clustered_data("output/lsi_sorted.txt", lsi_sorted)
    #lsi_max, lsi_min, lsi_med, lsi_mean = c.similarity_index(lsi_vec_sorted)
    print("LSI score =",lsi_score)
    #print("Max similarity =",lsi_max,"\nMin similarity =",lsi_min,"\nMedian similarity =",lsi_med,"\nMean similarity =",lsi_mean,"\n")

    # Clustering k means objects (LDA)
    print("\tLDA k-means :\nNumber of clusters =",num_topics,"\nNumber of resets =",num_resets,"\nVector Size =",lda_vec_size,"\nModel seed =",lda_seed)
    c.skl_kmeans(num_topics, 100)
    c.skl_kmeans_fit(lda_training_arrays)
    lda_classified = c.skl_kmeans_get_clusters(lda_sort_arrays)
    lda_sorted = c.sort(documents_to_sort, lda_classified, num_topics)
    lda_vec_sorted = c.sort(lda_sort_arrays, lda_classified, num_topics)
    lda_score = c.skl_score(lda_sort_arrays)
    c.write_clustered_data("output/lda_sorted.txt", lda_sorted)
    #lda_max, lda_min, lda_med, lda_mean = c.similarity_index(lda_vec_sorted)
    print("LDA score =",lda_score)
    #print("Max similarity =",lda_max,"\nMin similarity =",lda_min,"\nMedian similarity =",lda_med,"\nMean similarity =",lda_mean,"\n")

    # Clustering k means objects (Word2Vec)
    print("\tWord2Vec k-means :\nNumber of clusters =",num_topics,"\nNumber of resets =",num_resets,"\nVector Size =",vec_size)#,"\nModel seed =",w2v_seed)
    c.skl_kmeans(num_topics, 100)
    c.skl_kmeans_fit(w2v_training_arrays)
    w2v_classified = c.skl_kmeans_get_clusters(w2v_sort_arrays)
    w2v_sorted = c.sort(documents_to_sort, w2v_classified, num_topics)
    w2v_vec_sorted = c.sort(w2v_sort_arrays, w2v_classified, num_topics)
    w2v_score = c.skl_score(w2v_sort_arrays)
    c.write_clustered_data("output/w2v_sorted.txt", w2v_sorted)
    #w2v_max, w2v_min, w2v_med, w2v_mean = c.similarity_index(w2v_vec_sorted)
    print("Word2Vec score =",w2v_score)
    #print("Max similarity =",w2v_max,"\nMin similarity =",w2v_min,"\nMedian similarity =",w2v_med,"\nMean similarity =",w2v_mean,"\n")

    # Clustering k means objects (Doc2Vec)
    print("\tDoc2Vec k-means :\nNumber of clusters =",num_topics,"\nNumber of resets =",num_resets,"\nVector Size =",vec_size,"\nModel seed =",d2v_seed)
    c.skl_kmeans(num_topics, 100)
    c.skl_kmeans_fit(d2v_training_arrays)
    d2v_classified = c.skl_kmeans_get_clusters(d2v_sort_arrays)
    d2v_sorted = c.sort(documents_to_sort, d2v_classified, num_topics)
    d2v_vec_sorted = c.sort(d2v_sort_arrays, d2v_classified, num_topics)
    d2v_score = c.skl_score(d2v_sort_arrays)
    c.write_clustered_data("output/d2v_sorted.txt", d2v_sorted)
    #d2v_max, d2v_min, d2v_med, d2v_mean = c.similarity_index(d2v_vec_sorted)
    print("Doc2Vec score =",d2v_score)
    #print("Max similarity =",d2v_max,"\nMin similarity =",d2v_min,"\nMedian similarity =",d2v_med,"\nMean similarity =",d2v_mean,"\n")

    print("\n\n")
    c.graph_elbow(d2v_training_arrays, 1, 500, 10)

    """
    * Functions to make :
    * * K-means data preparation functions in model.py for all possible input data types
    * * Data translation functions in model.py that turns an input array of documents to the vectors that comprise them
    * * Analytics functions in a new file that determines how accurate the system is
    * * Full analysis script - provides functions that process from file to result
    * * * Sub function, load or train selected model
    """



    # Model the vector space better => classification (in LSI) is very inconsistent between k-means runs (indistinct topic modelling)

if __name__ == '__main__':
    main()





