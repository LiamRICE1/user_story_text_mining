# author : Liam RICE

from dataset import Dataset
from preprocessing import Preprocessing
from model import Model

def main():
	seed = 0

	dataset = Dataset()
	preprocessing = Preprocessing()
	model = Model()

	test_phrase = ["as a user, i would like to process something"]
	complex_token_query_phrase = preprocessing.simple_tokenize(test_phrase)
	simple_token_query_phrase = preprocessing.simple_tokenize(test_phrase)
	"""
	data = dataset.read_from_file("test/user_stories_dataset_1.txt")
	tokens = preprocessing.simple_tokenize(data)
	dictionary = preprocessing.to_dictionary(tokens)
	doc_term_matrix = preprocessing.to_doc_term_matrix(dictionary, tokens)
	lda = model.lda(doc_term_matrix, dictionary, seed=seed)

	value = preprocessing.to_doc_term_matrix(dictionary, preprocessing.simple_tokenize(["as a user, i would like to process something"]))
	print(value)
	print(lda[value[0]])

	data = dataset.read_from_file("test/user_stories_dataset_1.txt")
	tokens = preprocessing.simple_tokenize(data)
	dictionary = preprocessing.to_dictionary(tokens)
	doc_term_matrix = preprocessing.to_doc_term_matrix(dictionary, tokens)
	lsi = model.lsi(doc_term_matrix, dictionary, seed=seed)
	complex_dtm_query_phrase = preprocessing.to_doc_term_matrix(dictionary, complex_token_query_phrase)
	print(model.get_value(complex_dtm_query_phrase[0]))
	"""
	data = dataset.read_from_file("test/user_stories_dataset_1.txt")
	tokens = preprocessing.simple_tokenize_tag(data)
	d2v = model.doc2vec(tokens, seed=seed)
	print(sum(model.doc2vec_get_vector(simple_token_query_phrase[0])))


if __name__ == '__main__':
    main()
