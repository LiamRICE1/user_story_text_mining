#!/bin/bash

cd src
pdoc --force --html ./ --output-dir ../doc/pdoc --skip-errors
cd ..