#!/bin/bash

if [ $1 = "a" ]
then
	echo Beginning all tests...
	python -m unittest discover -s src -p "*_test.py"
	echo Tests complete.
fi



if [ $1 = "u" ]
then
	echo Beginning unit tests...
	python -m unittest discover -s src -p "*_unit_test.py"
	echo Tests complete.
fi
if [ $1 = "i" ]
then
	echo Beginning integration tests...
	python -m unittest discover -s src -p "*integration_test.py"
	echo Tests complete.
fi



if [ $1 = "d" ]
then
	echo Beginning dataset tests...
	python -m unittest discover -s src -p "dataset_unit_test.py"
	echo Tests complete.
fi
if [ $1 = "p" ]
then
	echo Beginning preprocessing tests...
	python -m unittest discover -s src -p "preprocessing_unit_test.py"
	echo Tests complete.
fi
if [ $1 = "m" ]
then
	echo Beginning model tests...
	python -m unittest discover -s src -p "model_unit_test.py"
	echo Tests complete.
fi
if [ $1 = "c" ]
then
	echo Beginning clustering tests...
	python -m unittest discover -s src -p "cluster_unit_test.py"
	echo Tests complete.
fi
if [ $1 = "ds" ]
then
	echo Beginning data structure tests...
	python -m unittest discover -s src -p "data_structures_unit_test.py"
	echo Tests complete.
fi
if [ $1 = "f" ]
then
	echo Beginning feature model tests...
	python -m unittest discover -s src -p "feature_model_unit_test.py"
	echo Tests complete.
fi