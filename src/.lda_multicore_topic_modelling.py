"""
row = ([(1, 0.047556244), (2, 0.020638717), (7, 0.12077141), (8, 0.3170589), (9, 0.27408746), (18, 0.14608242), (19, 0.044650115)], [(0, [7, 19]), (1, [7]), (2, []), (3, [7, 8]), (4, [8, 7, 9, 19]), (5, [18]), (6, [7, 8]), (7, [9, 1]), (8, [8, 7]), (9, [18]), (10, [9]), (11, []), (12, [7, 2]), (13, [9]), (14, [9, 19]), (15, [8, 9]), (16, [8, 19]), (17, []), (18, [9, 8]), (19, [9, 8, 19]), (20, [9, 19]), (21, [8, 2]), (22, [18, 19]), (23, [7, 19, 8]), (24, [1]), (25, [9]), (26, [9, 8]), (27, [9]), (28, [1]), (29, [8]), (30, [8, 2]), (31, [8]), (32, [8, 7]), (33, [19, 8, 2]), (34, [8, 18, 19]), (35, [9]), (36, [8]), (37, []), (38, [9]), (39, [8, 2]), (40, []), (41, [9, 8]), (42, [8, 18])], [(0, [(7, 0.74277157), (19, 0.2567167)]), (1, [(7, 0.99985045)]), (2, []), (3, [(7, 0.74248034), (8, 0.2573446)]), (4, [(7, 0.36597776), (8, 0.41226798), (9, 0.16278805), (19, 0.058917347)]), (5, [(18, 4.9998612)]), (6, [(7, 0.6066827), (8, 0.39326188)]), (7, [(1, 0.4994661), (9, 1.5001792)]), (8, [(7, 0.49012265), (8, 0.5080899)]), (9, [(18, 0.99989617)]), (10, [(9, 0.9981421)]), (11, []), (12, [(2, 0.012210809), (7, 0.98361665)]), (13, [(9, 0.99999195)]), (14, [(9, 0.92095387), (19, 0.07898211)]), (15, [(8, 0.64585614), (9, 0.3426304)]), (16, [(8, 0.94383425), (19, 0.05586334)]), (17, []), (18, [(8, 0.04371987), (9, 0.9562771)]), (19, [(8, 0.85886586), (9, 1.1307275), (19, 0.010353055)]), (20, [(9, 0.944901), (19, 0.055070296)]), (21, [(2, 0.015682386), (8, 0.98429376)]), (22, [(18, 0.98047817), (19, 0.01944498)]), (23, [(7, 0.8605256), (8, 0.061702203), (19, 0.07770055)]), (24, [(1, 0.9983602)]), (25, [(9, 0.9999927)]), (26, [(8, 0.03573848), (9, 0.96425635)]), (27, [(9, 0.99972856)]), (28, [(1, 0.99957895)]), (29, [(8, 0.99994016)]), (30, [(2, 0.019386636), (8, 0.97922295)]), (31, [(8, 0.9999576)]), (32, [(7, 0.08707678), (8, 0.9076448)]), (33, [(2, 0.121300526), (8, 0.35275045), (19, 0.52469563)]), (34, [(8, 0.4873234), (18, 0.46263394), (19, 0.04869263)]), (35, [(9, 0.9997247)]), (36, [(8, 0.9994698)]), (37, []), (38, [(9, 0.9999841)]), (39, [(2, 0.028575487), (8, 0.971379)]), (40, []), (41, [(8, 0.10304026), (9, 0.89688206)]), (42, [(8, 0.83610886), (18, 0.15000787)])])

for x in row:
	print(x[1][1])

row = sorted(row, key=lambda x: (x[1][0]), reverse=True) # trying to compare int and tuple?
"""

from gensim.test.utils import common_texts
from gensim.corpora.dictionary import Dictionary
import gensim
from gensim.models.ldamulticore import LdaMulticore
from gensim.models.coherencemodel import CoherenceModel
from dataset import *

# MEDIAN OPTIMAL VALUE = 47
# AVERAGE OPTIMAL VALUE = 44.5


def main():
	# constants
	MIN_TOPICS = 30
	MAX_TOPICS = 50
	NUM_ITERATIONS = 20


	print("\n\n\nLDA MULTICORE\n")

	# multicore test
	text = readFromFile("data/user_stories_dataset_1.txt")
	texts = [gensim.utils.simple_preprocess(str(sentence), deacc=True) for sentence in text]
	dictionary = Dictionary(texts)
	corpus = [dictionary.doc2bow(text) for text in texts]

	values = []
	for i in range(0,NUM_ITERATIONS):
		print("Iteration ",i+1,"/",NUM_ITERATIONS)
		coherence_values = []
		for num_topics in range(MIN_TOPICS, MAX_TOPICS+1):

			lda = LdaMulticore(corpus, id2word=dictionary, num_topics=num_topics, workers=16)
			coherencemodel = CoherenceModel(model=lda, texts=texts, dictionary=dictionary, coherence='c_v')
			coherence_values.append(coherencemodel.get_coherence())

			other_texts = [
				['computer', 'time', 'graph'],
				['survey', 'response', 'eps'],
				['human', 'system', 'computer'],
			]

			other_corpus = [dictionary.doc2bow(text) for text in other_texts]
			unseen_doc = other_corpus[0]

			vector = lda[unseen_doc]
			#print("Newly seen doc : ", vector)

			#print("Detected topics : ", lda.get_topics())

			#print("Topic distribution : ", lda.__getitem__(unseen_doc))

			topic_distributions = []
			for document in corpus:
				topic_distributions.append((sorted(lda[document], key=lambda x: x[1], reverse=True)))

		"""
			for i in range(len(lda.get_topics())):
				best_topics = []
				average = 0
				counter = 0
				for topic in topic_distributions:
					if(topic[0][0] == i):
						best_topics.append(text[counter] + " - " + topic[0][0].__str__() + " : " + topic[0][1].__str__())
						average += topic[0][1]
						counter+=1
				average = average / len(best_topics)

				writeStringsToFile(best_topics, "output/output"+i.__str__()+".txt")
				print("Average Confidence for topic ", i," : ", average)

		"""
		#print(coherence_values)

		counter = 0
		max_co = 0
		max_id = 0
		for coh in coherence_values:
			if(coh > max_co):
				max_co=coh
				max_id=counter
			counter+=1

		print("Max Coherence : ", max_co, "\nOptimum Value : ", max_id+MIN_TOPICS)
		values.append(max_id+MIN_TOPICS)

	values.sort()
	median = values[10]
	average = sum(values)/len(values)
	print("\n\nMedian : ",median, "\nAverage : ", average)


if __name__ == '__main__':
    main()

