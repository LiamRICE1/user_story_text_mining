from dataset import Dataset
from preprocessing import Preprocessing
from model import Model
from cluster import Cluster
from performance import Performance
from data_structures import *
from gensim.models.word2vec import LineSentence
from gensim.test.utils import datapath
from gensim.models.doc2vec import TaggedDocument
from nltk.corpus import stopwords
import nltk.corpus
from nltk.corpus import wordnet
import spacy



def main():
	from sklearn.datasets import fetch_20newsgroups
	newsgroups_train = fetch_20newsgroups(subset='train')


	d = Dataset()
	p = Preprocessing()
	m = Model()
	c = Cluster()
	pp = Performance()

	#"""
	print("Loading data...")
	t_docs = p.steam_to_line_sentence_dict("data/gitlab.txt", "data_dump.txt", 200000, 10000)
	ls = LineSentence("data/gitlab_line_sentence.txt")
	print("Data loaded.")
	#"""
	#"""
	print("\nLDA")
	docs = d.read_unformatted_from_file("data/user_story_datasets/g04-recycling.txt")
	p.preprocess_lsi_lda(docs, t_docs.document_dictionary)
	print("Training model...")
	lda,_ = m.lda_stream(ls, t_docs, num_topics=50)
	print("Model trained.")
	values = m.get_values(docs, 50)
	c.skl_kmeans(15)
	labels = c.skl_kmeans_fit_and_cluster(docs)
	score = c.skl_score(docs)
	final = c.sort(docs, labels, 15)
	print(score)
	c.write_clustered_data("output/lda_full_cluster.txt", final)
	c.graph_elbow_vec(docs, 2, 20, 1)
	#"""
	"""
	print("\nLSI")
	docs = d.read_unformatted_from_file("data/user_story_datasets/g04-recycling.txt")
	#t_docs = d.read_from_file("data/gitlab.txt")
	p.preprocess_lsi_lda(docs, docs)
	lsi,_ = m.lsi_stream(ls, t_docs, num_topics=100)
	values = m.get_values(docs, 100)
	c.skl_kmeans(15)
	labels = c.skl_kmeans_fit_and_cluster(docs)
	score = c.skl_score(docs)
	final = c.sort(docs, labels, 15)
	print(score)
	c.write_clustered_data("output/lsi_full_cluster.txt", final)
	c.graph_elbow_vec(docs, 2, 20, 1)
	#"""
	"""
	print("\nWord2Vec")
	docs = d.read_unformatted_from_file("data/user_story_datasets/g04-recycling.txt")
	p.preprocess_word2vec(docs, docs)
	w2v,_ = m.word2vec(docs)
	m.word2vec_load('glove-wiki-gigaword-300')
	print(docs.tokenized_documents[0])
	values = m.word2vec_get_vectors(docs, 100)
	print(w2v.wv.most_similar('employees', topn=10))
	c.skl_kmeans(14)
	labels = c.skl_kmeans_fit_and_cluster(docs)
	score = c.skl_score(docs)
	sorted_labels = c.sort(docs, labels, 14)
	print(score)
	c.write_clustered_data("output/word2vec_full_cluster.txt", sorted_labels)
	c.graph_elbow_vec(docs, 2, 20, 1)
	#"""
	"""
	t_docs = d.read_from_file("data/gitlab.txt")
	p.complex_tokenize_tag(t_docs, True, True, False, "s")
	p.complex_tokenize(t_docs, True, True, "s")
	print("Training...")
	d2v,_ = m.doc2vec(t_docs, 300, 20, False, False, False)
	print("Training complete.")
	#"""
	"""
	print("\nDoc2Vec")
	docs = d.read_unformatted_from_file("data/user_story_datasets/g04-recycling.txt")
	p.complex_tokenize_tag(docs, True, True, False, "s")
	p.complex_tokenize(docs, True, True, "s")
	m.doc2vec_stream("data/gitlab_line_sentence.txt")
	values = m.doc2vec_get_vectors(docs)
	c.skl_kmeans(15)
	labels = c.skl_kmeans_fit_and_cluster(docs)
	score = c.skl_score(docs)
	sorted_labels = c.sort(docs, labels, 15)
	print(score)
	c.write_clustered_data("output/doc2vec_full_cluster.txt", sorted_labels)
	print("Graph elbow :")
	c.graph_elbow_vec(docs, 5, 25, 1)
	#"""


def read_test():

	d = Dataset()
	p = Preprocessing()
	m = Model()
	c = Cluster()
	pp = Performance()

	import nltk
	import spacy
	from gensim.test.utils import common_texts

	docs = d.read_unformatted_from_file("data/user_story_datasets/g04-recycling.txt")
	s_docs = p.remove_subject(docs)
	action_words, object_words, subject_words = p.isolate_parts(s_docs)
	
	docs = d.read_unformatted_from_file("data/user_story_datasets/g04-recycling.txt")
	p.complex_tokenize(docs, remove_stopwords=False, stem=False, length='f')

	p.replace_docs_with_ngram_docs(docs)
	#parts = p.parts_of_user_stories(docs)
	
	#"""
	list_a, list_b, list_c, list_d = p._pous_classes(docs)
	print("A :", list_a)
	print("B :", list_b)
	print("C :", list_c)
	print("D :", list_d)

	trees = p.to_trees(docs)
	for i in range(len(trees)):
		if(i in list_b):
			s, o, a = p._fetch_b(trees[i], docs.tagged_documents[i], 0, trees[i])
			print("\n", docs.short_training_set()[i])
			print("Subject :", s)
			print("Object :", o)
			print("Actions :", a)
			trees[i].pretty_print()
	#"""
	"""
	for part in parts:
		print("\n")
		for sub in part:
			print(sub)
	#"""
	"""
	pop = add_synonyms_to_tokenized_documents(docs)
	for x in pop.tokenized_documents:
		print(x)
	#"""

def add_synonyms_to_tokenized_documents(documents, lang="en"):

	# FOR NOW, ONLY USES WORD2VEC. SHOULD BE UPGRADED WITH WORDNET?

	d = Dataset()
	p = Preprocessing()
	m = Model()
	c = Cluster()
	pp = Performance()

	print("\nLoading model...")
	model = m.word2vec_load('glove-wiki-gigaword-300')
	print("Model loaded.")

	import nltk.corpus
	from nltk.corpus import wordnet

	if(lang=="fr"):
		language="fra"
	else:
		language="eng"

	print("\nProcessing synsets...")

	for doc in documents.tokenized_documents:
		syns = []
		for word in doc:
			synonyms = []
			similars = model.most_similar(word)
			for word, similarity in similars:
				if(similarity >= 0.5):
					synonyms.append(word)
			syns.extend(synonyms)
			i+=1
			"""
			word_synsets = wordnet.synsets(word, lang=language)
			if(len(word_synsets) >= 1):
				word_synset = word_synsets[0]
				for syn in word_synsets:
					for i in syn.lemmas():
						print(model.similarity(word, i.name()))
						synonyms.append(i.name())
				print(synonyms)
				syns.extend(synonyms)
			else:
				print("n/a")
			"""
			# nltk wordnet topic domains
			# wordnet.synset('code.n.03').topic_domains()
			"""
			from collections import defaultdict
			from nltk.corpus import wordnet as wn

			domain2synsets = defaultdict(list)
			synset2domains = defaultdict(list)
			for i in open('wn-domains-3.2-20070223', 'r'):
			    ssid, doms = i.strip().split('\t')
			    doms = doms.split()
			    synset2domains[ssid] = doms
			    for d in doms:
			        domain2synsets[d].append(ssid)

			for ss in wn.all_synsets():
			    ssid = str(ss.offset).zfill(8) + "-" + ss.pos()
			    if synset2domains[ssid]:
			        print( ss, ssid, synset2domains[ssid])

			for dom in sorted(domain2synsets):
			    print(dom, domain2synsets[dom][:3])
			"""
		doc.extend(syns)
	print("Syncets generated.")

	return documents



def complex_tokenize_tag(self, documents, remove_stopwords=True, stem=True, pos_tag=False, length="s", lang="en"):
	"""
	## This function takes a list of documents and applies complex preprocessing using NLTK. Due to the nature of the pos-tagger, you cannot both stem and pos-tag the documents.
	### Args :
	* `src.data_structures.Documents` documents : the documents to which the processing needs to be applied.
	* bool remove_stopwords : default True. If true, this function removes english stopwords from the list of tokens
	* bool stem : default True. If true, this function stems the words.
	* bool pos_tag : default False. If true, this function assigns part-of-speach tagging on the words.
	* String length : short ("s") by default, can be set to long ("l") or full ("f"). If input is not one of those three, uses short by default.
	### Returns :
	* TaggedDocument[] returns an array of TaggedDocuments ready for word2vec or doc2vec processing.
	"""
	if(length == "l"):
		original_documents = documents.long_training_set()
	elif(length == "f"):
		original_documents = documents.full_training_set()
	else:
		original_documents = documents.short_training_set()
	if(pos_tag):
		stem = False

	if(lang == "fr"):
		nlp = spacy.load("fr_core_news_sm")
		stop = set(stopwords.words('french'))
	else:
		nlp = spacy.load("en_core_web_sm")
		stop = set(stopwords.words('english'))

	texts = []
	toks = []
	num = 0
	for doc in original_documents:
		processed = nlp(doc)
		processed_doc = []
		processed_toc = []
		tags = []
		for tok in processed:
			if(tok.pos_ != "PUNCT"):
				if((str(tok).lower() not in stop) or (not remove_stopwords)):
					if(stem):
						processed_doc.append(tok.lemma_.lower())
					else:
						processed_doc.append(str(tok).lower())
					if(pos_tag):
						tags.append(tok.pos_)
		if(tags == []):
			tags = [num]
		texts.append(TaggedDocument(processed_doc, tags))
		toks.append(processed_doc)
		num += 1
	documents.tagged_documents = texts
	documents.tokenized_documents = toks
	return texts


def add_synonyms_to_parts_of_phrase(pop, lang="en"):

	# FOR NOW, ONLY USES WORD2VEC. SHOULD BE UPGRADED WITH WORDNET?

	d = Dataset()
	p = Preprocessing()
	m = Model()
	c = Cluster()
	pp = Performance()

	print("\nLoading model...")
	model = m.word2vec_load('glove-wiki-gigaword-300')
	print("Model loaded.")

	if(lang=="fr"):
		language="fra"
	else:
		language="eng"

	print("\nProcessing synsets...")

	total_length = 0
	for doc in pop:
		total_length += len(doc)
	count = 0
	x = total_length/20
	i = 0
	print("[", end="", flush=True)

	for document in pop:
		num = 0
		for words in document:
			if(i >= x * count):
				print("=", end="", flush=True)
				count += 1
			if(num > 0):
				synonyms = []
				for word in words:
					try:
						similars = model.most_similar(word)
						for word, similarity in similars:
							if(similarity >= 0.6):
								synonyms.append(word)
					except:
						pass
				words.extend(synonyms)
			i += 1
			num += 1
			"""
			word_synsets = wordnet.synsets(word, lang=language)
			if(len(word_synsets) >= 1):
				word_synset = word_synsets[0]
				for syn in word_synsets:
					for i in syn.lemmas():
						print(model.similarity(word, i.name()))
						synonyms.append(i.name())
				print(synonyms)
				syns.extend(synonyms)
			else:
				print("n/a")
			"""
			# nltk wordnet topic domains
			# wordnet.synset('code.n.03').topic_domains()
			"""
			from collections import defaultdict
			from nltk.corpus import wordnet as wn

			domain2synsets = defaultdict(list)
			synset2domains = defaultdict(list)
			for i in open('wn-domains-3.2-20070223', 'r'):
			    ssid, doms = i.strip().split('\t')
			    doms = doms.split()
			    synset2domains[ssid] = doms
			    for d in doms:
			        domain2synsets[d].append(ssid)

			for ss in wn.all_synsets():
			    ssid = str(ss.offset).zfill(8) + "-" + ss.pos()
			    if synset2domains[ssid]:
			        print( ss, ssid, synset2domains[ssid])

			for dom in sorted(domain2synsets):
			    print(dom, domain2synsets[dom][:3])
			"""

	print("]")
	print("Syncets generated.")

	return pop


def add_synonyms_to_parts_of_phrase_wordnet(pop, lang="en", min_similarity_index=0.6, use_word2vec=True, use_wordnet=True, get_hypernyms=True, max_synonyms=5):

	d = Dataset()
	p = Preprocessing()
	m = Model()
	c = Cluster()
	pp = Performance()

	if(use_word2vec):
		print("\nLoading model...")
		model = m.word2vec_load('glove-wiki-gigaword-300')
		print("Model loaded.")

	if(lang=="fr"):
		language="fra"
	else:
		language="eng"

	print("\nProcessing synsets...")

	total_length = 0
	for doc in pop:
		total_length += len(doc)
	count = 0
	x = total_length/20
	i = 0
	print("[", end="", flush=True)

	result = []
	for document in pop:
		doc_array = []
		num = 0
		for words in document:
			if(i >= x * count):
				print("=", end="", flush=True)
				count += 1
			if(num > 0):
				synonyms = [w for w in words]
				for word in words:
					"""
					if(use_word2vec):
						try:
							similars = model.most_similar(word)
							for word, similarity in similars:
								if(similarity >= 0.6):
									synonyms.append(word)
						except:
							pass
					"""
					if(use_wordnet):
						try:
							word_synsets = wordnet.synsets(word, lang=language)
							if(len(word_synsets) > 0):
								word_synset = word_synsets[0]
								for syn in word_synsets:
									num_syns = 0
									for s in syn.lemmas():
										if(use_word2vec):
											if(model.similarity(word, s.name()) > min_similarity_index and num_syns < max_synonyms):
												synonyms.append(s.name())
										else:
											if(num_syns < max_synonyms):
												synonyms.append(s.name())
												num_syns += 1
						except:
							pass
					if(get_hypernyms):
						try:
							word_synsets = wordnet.synsets(word, lang=language)
							if(len(word_synsets) > 0):
								wordnet_word = wordnet.synset(word_synsets[0].name())
								hypernyms = wordnet_word.hypernyms()
								for hypernym in hypernyms:
									for lemma in hypernyms.lemmas():
										synonyms.append(lemma.name())
						except:
							pass
				doc_array.append(synonyms)
			i += 1
			num += 1
		result.append(doc_array)
		"""
		word_synsets = wordnet.synsets(word, lang=language)
		if(len(word_synsets) >= 1):
			word_synset = word_synsets[0]
			for syn in word_synsets:
				for i in syn.lemmas():
					print(model.similarity(word, i.name()))
					synonyms.append(i.name())
			print(synonyms)
			syns.extend(synonyms)
		else:
			print("n/a")
		"""
		# nltk wordnet topic domains
		# wordnet.synset('code.n.03').topic_domains()
		"""
		from collections import defaultdict
		from nltk.corpus import wordnet as wn

		domain2synsets = defaultdict(list)
		synset2domains = defaultdict(list)
		for i in open('wn-domains-3.2-20070223', 'r'):
		    ssid, doms = i.strip().split('\t')
		    doms = doms.split()
		    synset2domains[ssid] = doms
		    for d in doms:
		        domain2synsets[d].append(ssid)

		for ss in wn.all_synsets():
		    ssid = str(ss.offset).zfill(8) + "-" + ss.pos()
		    if synset2domains[ssid]:
		        print( ss, ssid, synset2domains[ssid])

		for dom in sorted(domain2synsets):
		    print(dom, domain2synsets[dom][:3])
		"""

	print("]")
	print("Syncets generated.")

	final = []
	for document in result:
		new_doc = []
		for set_words in document:
			added_words = []
			new_set = []
			for word in set_words:
				go = True
				for check in added_words:
					if(check == word):
						go = False
				if(go):
					new_set.append(word)
					added_words.append(word)
			new_doc.append(new_set)
		final.append(new_doc)
	return final


import nltk

class UnigramChunker(nltk.ChunkParserI):
	def __init__(self, train_sents):
		train_data = [[(t,c) for w,t,c in nltk.chunk.tree2conlltags(sent)] for sent in train_sents]
		self.tagger = nltk.UnigramTagger(train_data)

	def parse(self, sentence):
		pos_tags = [pos for (word, pos) in sentence]
		tagged_pos_tags = self.tagger.tag(pos_tags)
		chunktags = [chunktag for (pos, chunktag) in tagged_pos_tags]
		conlltags = [(word, pos, chunktag) for ((word, pos), chunktag) in zip(sentence, chunktags)]
		return nltk.chunk.conlltags2tree(conlltags)


def pop_nltk(docs):
	from nltk.corpus import conll2000

	tagged_sentences = []
	for doc in docs.tagged_documents:
		tagged_sentence = []
		for word, tag in zip(doc[0], doc[1]):
			tagged_sentence.append((word, tag))
		tagged_sentences.append(tagged_sentence)

	trees = []
	for doc in tagged_sentences:
		test_sents = conll2000.chunked_sents('test.txt', chunk_types=['NP'])
		rp = nltk.RegexpParser(r"NP: {<[CDJNP].*>+}")
		trees.append(rp.parse(doc))
	return trees
	# Try using nltk with a Unigram Chunker to create a similar tree to isolate parts. Might be more consistent?



def presentation_parts_of_user_stories():
	d = Dataset()
	p = Preprocessing()

	docs = d.read_unformatted_from_file("data/user_story_datasets/g11-nsf.txt")
	p.complex_tokenize(docs, remove_stopwords=False, stem=False, length='f')
	p.complex_tokenize_tag(docs, remove_stopwords=False, stem=False, pos_tag=True, length='f')
	#p.replace_docs_with_ngram_docs(docs)
	# HOW AND WHEN TO USE NGRAMS ?
	trees = p.to_trees(docs)

	pad = p.parts_of_user_stories(docs)
	#pop = add_synonyms_to_parts_of_phrase(pad)
	for i, x, doc, tree, tag_doc in zip(range(len(pad)), pad, docs.short_training_set(), trees, docs.tagged_documents):
		print("\n"+str(i)+" - "+doc)
		tree.pretty_print()


def presentation_classification():
	d = Dataset()
	m = Model()
	c = Cluster()
	pp = Performance()

	docs = d.read_unformatted_from_file("data/user_story_datasets/g04-recycling.txt")
	training = d.read_unformatted_from_file("data/user_story_training_dataset.txt")

	print("Processing docs to classify...")
	tagged_docs = prepare_parts_of_user_stories(docs, False, False, True, False, True, True, "en")
	print("Processing training docs...")
	prepare_parts_of_user_stories(training, False, False, True, False, True, True, "en")

	"""
	for doc, toks in zip(docs.short_training_set(), docs.tokenized_documents):
		print(doc)
		print(toks)
	#"""

	#"""
	lda,_ = m.lda(training, 30, iterations=20)
	vectors = m.get_values(docs, 30)
	#"""

	"""
	print("Training model...")
	d2v = m.doc2vec(training, 30, 20, True, True, True)
	print("Generating vectors...")
	vectors = m.doc2vec_get_vectors(docs)
	#"""

	print("Generating elbow graph...")
	c.graph_elbow_vec(docs, 2, 40)
	c.skl_kmeans(20, 100, 5000)
	labels = c.skl_kmeans_fit_and_cluster(docs)
	sorted_data = c._sort(docs, labels, 20, tagged_docs)

	#"""
	for topic in sorted_data:
		print("\n\n\n")
		for i, (doc, toks, tags) in zip(range(len(topic)), topic):
			print(i, ":", doc)
			arr = []
			for word, tag in tags:
				if word in toks:
					arr.append((word, tag))
			#print(arr)
	#"""


def verification(pop, docs):
	p = Preprocessing()
	documents = []
	for doc, line, tags, document_doc in zip(pop, docs.short_training_set(), docs.tagged_documents, docs):
		empty_section_count = 0
		num = 0
		for section in doc:
			if(section == [] and num < 3):
				empty_section_count += 1
			num += 1
		if(empty_section_count > 1):
			arr = []
			for word, tag in zip(tags[0], tags[1]):
				arr.append((word, tag))
			documents.append(document_doc)
	new_docs = Documents(documents)
	p.parts_of_user_stories(new_docs, debug=True)


def prepare_parts_of_user_stories(docs, subject=False, actions=False, objects=True, others=False, synonyms=False, remove_stopwords=True, lang="en"):
	"""
	## This function preprocesses the input documents and splits them into parts of user stories in preparation for being processed by the model.
	### Args :
	* `src.data_structures.Documents` docs : the documents to preprocess and split up.
	* @Optional bool subject : determines whether the subjects of the user stories will be used in the training documents.
	* @Optional bool actions : determines whether the actions of the user stories will be used in the training documents.
	* @Optional bool objects : determines whether the objects of the user stories will be used in the training documents.
	* @Optional bool others : determines wheter the rest of the user stories will be used in the training documents.
	* @Optional bool synonyms : determines wheter the documents will be supplemented by synonym tokens.
	* @Optional bool remove_stopwords : determines whether the documents will have their stopwords removed.
	* @Optional string lang : selection for the stopwords language, "en" for english and "fr" for french, which are the only currently supported languages.
	### Returns :
	* (string, string)[][] : an array of tuple arrays, each tuple array reprisenting a document and each tuple containing a word and its corresponding tag.
	"""
	p = Preprocessing()
	pop = p.parts_of_user_stories(docs)
	#verification(pop, docs)
	tagged_docs = docs.tagged_documents
	# remove stopwords
	if(lang=="fr"):
		stop = set(stopwords.words("french"))
	else:
		stop = set(stopwords.words('english'))
	pod = []
	if(remove_stopwords):
		for document in pop:
			doc_val = []
			for clss in document:
				temp = [i for i in clss if i not in stop]
				doc_val.append(temp)
			pod.append(doc_val)
	else:
		pod = pop
	if(synonyms):
		#pad = add_synonyms_to_parts_of_phrase(pod)
		pad = add_synonyms_to_parts_of_phrase_wordnet(pod, "en", 0.6, True, True, True)
	else:
		pad = pod
	new_tok_docs = []
	new_tag_docs = []
	for i, pa in zip(range(len(pad)), pad):
		temp = []
		if(subject):
			temp.extend(pa[0])
		if(actions):
			temp.extend(pa[1])
		if(objects):
			temp.extend(pa[2])
		if(others):
			temp.extend(pa[3])
		new_tok_docs.append(temp)
		new_tag_docs.append(TaggedDocument(temp, [i]))
	docs.tokenized_documents = new_tok_docs
	docs.tagged_documents = new_tag_docs
	p.to_dictionary(docs)
	p.to_doc_term_matrix(docs)
	return tagged_docs


if __name__ == '__main__':
	presentation_parts_of_user_stories()